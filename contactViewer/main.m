//
//  main.m
//  contactViewer
//
//  Created by Khaled Qudwa on 8/30/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
