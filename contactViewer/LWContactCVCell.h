//
//  LWContactCVCell.h
//  contactViewer
//
//  Created by Khaled Qudwa on 8/30/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWContactCVCell : UICollectionViewCell
{
    
}
@property (strong, nonatomic) NSDictionary *contactRecord;

@property (strong, nonatomic) UIImageView *contactImage;
@property (strong, nonatomic) UILabel *emptyImageLbl;
@property (strong, nonatomic) UILabel *contactNameLbl;

@end
