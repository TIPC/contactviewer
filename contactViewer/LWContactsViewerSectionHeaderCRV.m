//
//  LWContactsViewerSectionHeaderCRV.m
//  contactViewer
//
//  Created by Khaled Qudwa on 8/30/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import "LWContactsViewerSectionHeaderCRV.h"

@implementation LWContactsViewerSectionHeaderCRV
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.sectionLbl = UILabel.new;
        self.sectionLbl.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
        UIViewAutoresizingFlexibleWidth |
        UIViewAutoresizingFlexibleRightMargin ;


        self.sectionLbl.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        self.sectionLbl.textAlignment = NSTextAlignmentLeft;
        self.sectionLbl.textColor = [UIColor darkGrayColor];
        self.sectionLbl.font = [UIFont boldSystemFontOfSize:15.0];
        self.sectionLbl.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.9];
        [self addSubview:self.sectionLbl];
    }
    return self;
}
@end
