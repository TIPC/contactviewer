//
//  LWConfig.h
//  contactViewer
//
//  Created by Khaled Qudwa on 8/30/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#ifndef contactViewer_LWConfig_h
#define contactViewer_LWConfig_h

#define AppDefaultColor [UIColor colorWithRed:0.447059 green:0.407843 blue:0.721569 alpha:1.0]
typedef enum : NSUInteger {
    GroupFirstName,
    GroupLastName
} GroupContactField;


#endif
