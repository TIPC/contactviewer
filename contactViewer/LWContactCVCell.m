//
//  LWContactCVCell.m
//  contactViewer
//
//  Created by Khaled Qudwa on 8/30/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import "LWContactCVCell.h"
#import "LWConfig.h"
@implementation LWContactCVCell
@synthesize contactRecord = _contactRecord;
@synthesize contactImage, emptyImageLbl, contactNameLbl;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contactImage = [UIImageView new];
        CGFloat frameWidth = frame.size.width;
        self.contactImage.frame = CGRectMake(10, 10,
                                             (frameWidth-20.0), (frameWidth-20.0));
        self.contactImage.layer.cornerRadius = self.contactImage.frame.size.width/2.0;
        self.contactImage.contentMode = UIViewContentModeScaleAspectFill;
        self.contactImage.layer.masksToBounds = YES;
        self.contactImage.clipsToBounds = YES;
        [self addSubview:self.contactImage];
        
        self.emptyImageLbl = [UILabel new];
        self.emptyImageLbl.frame = self.contactImage.frame;
        self.emptyImageLbl.layer.cornerRadius = self.contactImage.layer.cornerRadius;
        self.emptyImageLbl.layer.masksToBounds = YES;
        self.emptyImageLbl.clipsToBounds = YES;
        self.emptyImageLbl.backgroundColor = AppDefaultColor;
        self.emptyImageLbl.textAlignment = NSTextAlignmentCenter;
        self.emptyImageLbl.textColor = [UIColor whiteColor];
        self.emptyImageLbl.font = [UIFont boldSystemFontOfSize:20.0];
        [self addSubview:self.emptyImageLbl];
        
        self.contactNameLbl = [UILabel new];
        self.contactNameLbl.textAlignment = NSTextAlignmentCenter;
        self.contactNameLbl.frame = CGRectMake(5, CGRectGetMaxY(self.contactImage.frame)+5,
                                               frame.size.width-10.0, 50);
        self.contactNameLbl.font = [UIFont boldSystemFontOfSize:12.0];
        self.contactNameLbl.lineBreakMode = NSLineBreakByWordWrapping;
        self.contactNameLbl.numberOfLines = 2.0;
        [self addSubview:self.contactNameLbl];
    }
    return self;
}
-(void)setContactRecord:(NSDictionary *)contactRecord
{
    _contactRecord = contactRecord;
    self.contactNameLbl.text = [NSString stringWithFormat:@"%@ %@",_contactRecord[@"firstName"], _contactRecord[@"lastName"]];

    if ([_contactRecord[@"image"] isKindOfClass:[NSNull class]])
    {
        self.contactImage.hidden = YES;
        self.emptyImageLbl.hidden = NO;
        
        NSString *firstLetter = @"";
        if ([_contactRecord[@"firstName"] length] > 0)
            firstLetter = [_contactRecord[@"firstName"] substringToIndex:1];
        NSString *lastLetter = @"";
        if ([_contactRecord[@"lastName"] length] > 0)
            lastLetter = [_contactRecord[@"lastName"] substringToIndex:1];

        self.emptyImageLbl.text = [[NSString stringWithFormat:@"%@%@",
                                   firstLetter, lastLetter] uppercaseString];
        
        
    }
    else
    {
        self.contactImage.hidden = NO;
        self.emptyImageLbl.hidden = YES;
        self.contactImage.image = _contactRecord[@"image"];
    }
    
}
@end
