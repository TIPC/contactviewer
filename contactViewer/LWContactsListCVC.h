//
//  LWContactsListCVC.h
//  contactViewer
//
//  Created by Khaled Qudwa on 8/30/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LWConfig.h"
@interface LWContactsListCVC : UICollectionViewController
@property (nonatomic) GroupContactField groupField;
@end
