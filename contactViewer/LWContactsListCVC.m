//
//  LWContactsListCVC.m
//  contactViewer
//
//  Created by Khaled Qudwa on 8/30/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import "LWContactsListCVC.h"
@import AddressBook;
#import "LWContactsViewerSectionHeaderCRV.h"
#import "LWContactCVCell.h"
@interface LWContactsListCVC ()
{

    BOOL emptyAddressBook;
    BOOL accessDeniedAddressBook;
    NSString *sortKey;
}
@property (strong, nonatomic) NSMutableDictionary *contactList;
@property (strong, nonatomic) NSArray *setionsList;
@end

@implementation LWContactsListCVC
@synthesize contactList, setionsList, groupField = _groupField;

static NSString * const reuseHeaderIdentifier = @"sectionHeader";
static NSString * const reuseCellIdentifier = @"contactCell";


-(instancetype)init
{
    self = [super initWithCollectionViewLayout:self.layout];
    if (self)
    {
        self.contactList = NSMutableDictionary.new;
        self.setionsList = @[];
        emptyAddressBook = NO;
        accessDeniedAddressBook = NO;
        self.groupField = GroupLastName;
        
    }
    
    return self;
}

-(void)setGroupField:(GroupContactField)groupField
{
    _groupField = groupField;
    if (_groupField == GroupFirstName)
        sortKey = @"lastName";
    
    if (_groupField == GroupLastName)
        sortKey = @"firstName";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Sockam Contact Viewer";
    self.view.backgroundColor = self.collectionView.backgroundColor =
    [UIColor whiteColor];
    
    [self.collectionView registerClass:[LWContactsViewerSectionHeaderCRV class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:reuseHeaderIdentifier];

    [self.collectionView registerClass:[LWContactCVCell class]
            forCellWithReuseIdentifier:reuseCellIdentifier];

    [self fetchContacts];
    
}

-(UICollectionViewFlowLayout *)layout
{
    UICollectionViewFlowLayout *flowLayout;
    flowLayout = UICollectionViewFlowLayout.new;
    flowLayout.minimumLineSpacing = 0.0;
    flowLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    flowLayout.itemSize = CGSizeMake(80, 120);
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.headerReferenceSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 30);
    return flowLayout;
}



#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (emptyAddressBook || accessDeniedAddressBook)
        return 1;
    
    return self.setionsList.count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (emptyAddressBook || accessDeniedAddressBook)
        return 0;
    
    NSString *sectionKey = self.setionsList[section];
    return [self.contactList[sectionKey] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LWContactCVCell *cell = [collectionView
                             dequeueReusableCellWithReuseIdentifier:reuseCellIdentifier
                             forIndexPath:indexPath];
    
    NSString *sectionKey = self.setionsList[indexPath.section];
    cell.contactRecord = self.contactList[sectionKey][indexPath.row];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
          viewForSupplementaryElementOfKind:(NSString *)kind
                                atIndexPath:(NSIndexPath *)indexPath
{
    LWContactsViewerSectionHeaderCRV *resuableView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        resuableView = [collectionView
                        dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                        withReuseIdentifier:@"sectionHeader"
                        forIndexPath:indexPath];
        if (emptyAddressBook || accessDeniedAddressBook)
        {
            resuableView.sectionLbl.backgroundColor = [UIColor clearColor];
            resuableView.sectionLbl.font = [UIFont boldSystemFontOfSize:18.0];
            resuableView.sectionLbl.textAlignment = NSTextAlignmentCenter;
            resuableView.sectionLbl.lineBreakMode = NSLineBreakByWordWrapping;
            resuableView.sectionLbl.numberOfLines = 2.0;

            if (emptyAddressBook)
                resuableView.sectionLbl.text = @"Ops, You Don't Have any contacts";
            if (accessDeniedAddressBook)
                resuableView.sectionLbl.text = @"Please allow our app to access your contacts";
            
            CGRect tempFrame = resuableView.sectionLbl.frame;
            tempFrame.size.height = 80.0;
            resuableView.sectionLbl.frame = tempFrame;
       
            
        }
        else
        {
            resuableView.sectionLbl.text = [@"  " stringByAppendingString:self.setionsList[indexPath.section]];
        }
    }
    return resuableView;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - Fetch and sort contacts

-(void)fetchContacts
{
    ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil),
                                             ^(bool granted, CFErrorRef error)
    {
        accessDeniedAddressBook = !granted;
        if (granted)
        {
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               [self loadContactFromAddressBook];
                               emptyAddressBook = (contactList.count == 0);
                               if (!emptyAddressBook)
                               {
                                   [self sortContacts];
                                   self.setionsList = [self.contactList.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                               }
                               [self.collectionView reloadData];
                           });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });
        }
    });
}


-(void)loadContactFromAddressBook
{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, nil);
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
    NSInteger contactCount = CFArrayGetCount(people);
    for(int i = 0; i <  contactCount; i++)
    {
        ABRecordRef ref = CFArrayGetValueAtIndex(people, i);
        NSString *firstName = (__bridge NSString *)ABRecordCopyValue(ref,
                                                                    kABPersonFirstNameProperty);
        NSString *lastName = (__bridge NSString *)ABRecordCopyValue(ref, kABPersonLastNameProperty);
        
        UIImage *contactImage = nil;
        CFDataRef imageData = ABPersonCopyImageData(ref);
        contactImage = [UIImage imageWithData:(__bridge NSData *)imageData];
        //CFRelease(imageData);
        
        NSDictionary *contactRecord = @{
                                        @"firstName" : [self filterStringNull:firstName],
                                        @"lastName" : [self filterStringNull:lastName],
                                        @"image" : (contactImage == nil)? NSNull.new : contactImage
                                        };
        NSString *sectionKey = @"";
        if ([contactRecord[sortKey] length] > 0)
            sectionKey = [contactRecord[sortKey] substringToIndex:1];
        sectionKey = [sectionKey uppercaseString];
        if ([self.contactList.allKeys indexOfObject:sectionKey] == NSNotFound)
        {
            self.contactList[sectionKey] = [NSMutableArray new];
        }
        
        [self.contactList[sectionKey] addObject:contactRecord];
    }
    CFRelease(addressBook);
    CFRelease(people);

}
-(NSString *)filterStringNull:(NSString *)str
{
    str = [NSString stringWithFormat:@"%@",str];
    if([str isKindOfClass:[NSNull class]]) return @"";
    if(str == nil) return @"";
    
    if([str isEqualToString:@"(null)"]) return @"";
    if([str isEqualToString:@"(NULL)"]) return @"";
    if([str.lowercaseString rangeOfString:@"null" options:NSRegularExpressionSearch].location != NSNotFound) return  @"";
    
    return [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(void)sortContacts
{
    for (NSString *contactSectionKey in contactList)
    {
        NSMutableArray *contactSection = contactList[contactSectionKey];
        [contactSection sortUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2)
        {
            return [obj1[sortKey] compare:obj2[sortKey] options:NSCaseInsensitiveSearch];
        }];
    }

}



@end
